FROM python:3

WORKDIR /app
COPY rcache.py rcache.py
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt

CMD ["python", "rcache.py"]
